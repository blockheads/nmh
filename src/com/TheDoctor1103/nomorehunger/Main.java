package com.TheDoctor1103.nomorehunger;

import java.io.File;
import java.io.InputStream;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Main 
    extends JavaPlugin 
        implements Listener
{
	public final Logger logger = this.getLogger();
	private static FileConfiguration Config;
	private File configurationFile;
	
	public void onDisable()
	{
		Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[NoMoreHunger] has been disabled!" + ChatColor.RESET);
	}
	
	public void onEnable()
	{
		logTitle();
	    getServer().getPluginManager().registerEvents(new NMHEntityListener(), this);
	    getServer().getPluginManager().registerEvents(new NMHPlayerListener(), this);
	    loadConfig();
	    saveConfig();
	}
	
	public void logTitle()
	{
		Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "----------------------------------------");
		Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[NoMoreHunger] v" + getDescription().getVersion() + " has been enabled!");
		Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "----------------------------------------" + ChatColor.RESET);
	}
	
	  @SuppressWarnings("deprecation")
	public void loadConfig() 
	  {
		    if (this.configurationFile == null) 
		    {
		      this.configurationFile = new File(getDataFolder(), "config.yml");
		    }
		    Config = YamlConfiguration.loadConfiguration(this.configurationFile);
		    InputStream defConfigStream = getResource("config.yml");
		    if (defConfigStream != null) {
		      YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
		      Config.setDefaults(defConfig);
		    }
		  }
	  
	@EventHandler(priority=EventPriority.HIGH)
	  public static double grabFoodHealth(PlayerInteractEvent event) 
	  {
		double health = 0.0;
		ItemStack item = event.getItem();
		  if (event.getItem() == null)
		  {
		  }
		  
		    if (item.equals(Material.AIR))
		    {
		      return 0.0;
		    }
		    if (item.equals(Material.GOLDEN_APPLE) 
		    		&& item.getData().equals((short) 1) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("enchanted_golden_apple");
		         return health;
		    }
		    if (item.equals(Material.RAW_FISH) 
		    		&& item.getData().equals((short) 1) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("raw_salmon");
		         return health;
		    }
		    if (item.equals(Material.RAW_FISH) 
		    		&& item.getData().equals((short) 2) 
		    		&& item.getAmount() ==  1)
		    {
			      health = Config.getDouble("clownfish");
			      return health;
			}
		    if (item.equals(Material.RAW_FISH) 
		    		&& item.getData().equals((short) 3) 
		    		&& item.getAmount() == 1)
		    {
			      health = Config.getDouble("pufferfish");
			      return health;
			}
		    if (item.equals(Material.COOKED_FISH) 
		    		&& item.getData().equals((short) 1) 
		    		&& item.getAmount() == 1)
		    {
			      health = Config.getDouble("cooked_salmon");
			      return health;
			}
		    if (item.equals(Material.GOLDEN_APPLE) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		    	 health = Config.getDouble("golden_apple");
		    	 return health;
		    }
		    if (item.equals(Material.MELON) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("melon");
		         return health;
		    }
		    if (item.equals(Material.COOKED_CHICKEN) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("cooked_chicken");
		         return health;
		    }
		    if (item.equals(Material.COOKIE) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("cookie");
		         return health;
		    }
		    if (item.equals(Material.MUSHROOM_SOUP) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("mushroom_soup");
		         return health;
		    }
		    if (item.equals(Material.GRILLED_PORK) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("grilled_pork");
		         return health;
		    }
		    if (item.equals(Material.PORK) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("raw_porkchop");
		         return health;
		    }
		    if (item.equals(Material.BREAD) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("bread");
		         return health;
		    }
		    if (item.equals(Material.COOKED_FISH) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("cooked_fish");
		         return health;
		    }
		    if (item.equals(Material.RAW_FISH) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("raw_fish");
		         return health;
		    }
		    if (item.equals(Material.RAW_CHICKEN) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("raw_chicken");
		         return health;
		    }
		    if (item.equals(Material.ROTTEN_FLESH) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("rotten_flesh");
		         return health;
		    }
		    if (item.equals(Material.RAW_BEEF) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("raw_beef");
		         return health;
		    }
		    if (item.equals(Material.COOKED_BEEF) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("steak");
		         return health;
		    }
		    if (item.equals(Material.APPLE) 
		    		&& item.getData().equals((short) 0))
		    {
		         health = Config.getDouble("apple");
		         return health;
		    }
		    if (item.equals(Material.POTATO) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("potato");
		         return health;
		    }
		    if (item.equals(Material.BAKED_POTATO) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("baked_potato");
		         return health;
		    }
		    if (item.equals(Material.POISONOUS_POTATO) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("poison_potato");
		         return health;
		    }
		    if (item.equals(Material.CARROT) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("carrot");
		         return health;
		    }
		    if (item.equals(Material.GOLDEN_CARROT) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("golden_carrot");
		         return health;
		    }
		    if (item.equals(Material.PUMPKIN_PIE) 
		    		&& item.getData().equals((short) 0) 
		    		&& item.getAmount() == 1)
		    {
		         health = Config.getDouble("pumpkin_pie");
		         return health;
		    }
			return 0.0;
		  }
	  
		  public static double getCakeHealth() 
		  {
		    double health = Config.getInt("cake");
		    return health;
		  }

		public static Double grabFoodHealth(ItemStack item)
		{
			return null;
		}

}