package com.TheDoctor1103.nomorehunger;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class NMHEntityListener 
    implements Listener
    {

	  @EventHandler(priority=EventPriority.HIGHEST)
	  public void onEntityDamage(EntityDamageEvent event) 
	  {
	    if ((event.getCause() != EntityDamageEvent.DamageCause.STARVATION) || (!(event.getEntity() instanceof Player)))
	    {
	      return;
	    }
	    event.setCancelled(true);
	  }
	  
	  @EventHandler(priority=EventPriority.HIGHEST)
	  public void onEntityRegainHealth(EntityRegainHealthEvent event)
	  {
	    if (!(event.getEntity() instanceof Player))
	    {
	      return;
	    }
	    if ((event.getRegainReason() == EntityRegainHealthEvent.RegainReason.SATIATED) && ((event.getEntity() instanceof Player)))
        {
	      event.setCancelled(true);
	    }
	  }
	  @EventHandler(priority=EventPriority.HIGHEST)
	  public void onEntityHunger (FoodLevelChangeEvent event)
	  {
		  event.setCancelled(true);
	  }
}
