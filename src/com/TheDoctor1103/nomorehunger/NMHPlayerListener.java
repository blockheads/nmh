package com.TheDoctor1103.nomorehunger;

import net.minecraft.server.v1_7_R3.MobEffect;
import net.minecraft.server.v1_7_R3.MobEffectList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_7_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class NMHPlayerListener
    implements Listener
    {
@EventHandler(priority=EventPriority.HIGHEST)
  public void onPlayerInteract(PlayerInteractEvent event)
  {
    Action action = event.getAction();
    if ((action == Action.LEFT_CLICK_AIR) || (action == Action.LEFT_CLICK_BLOCK)) 
    {
      return;
    }
    if (((action == Action.RIGHT_CLICK_BLOCK) && (event.getClickedBlock().getType().equals(Material.CAKE_BLOCK)))) 
    {
      onPlayerRightClickCake(event);
    }
    if ((action == Action.RIGHT_CLICK_AIR) || ((action == Action.RIGHT_CLICK_BLOCK)))
    {
      onPlayerRightClick(event);
      Bukkit.broadcastMessage("Right Click Event");
    }
  }

  @SuppressWarnings("deprecation")
@EventHandler(priority=EventPriority.HIGHEST)
  private void onPlayerRightClick(PlayerInteractEvent event)
  {
	if (event.getItem() == null)
    {
    	return;
    }
	Player player = event.getPlayer();
    ItemStack item = event.getItem();
    String itemName = event.getItem().getType().toString();

    double health = Main.grabFoodHealth(event);
    
    @SuppressWarnings("unused")
	int meta = event.getItem().getData().getData();
    
    if (!eatingAllowed(player, health) || health == 0.0)
    {
    	Bukkit.broadcastMessage("Eating Not Allowed! :(");
    	return;
    }
    	
    setHealth(player, health);
    if (item.equals(Material.MUSHROOM_SOUP) 
    		&& item.getData().equals((short) 0) 
    		&& item.getAmount() == 1)
    {
      PlayerInventory inventory = player.getInventory();
      ItemStack bowl = new ItemStack(281, 1);
      inventory.addItem(new ItemStack[] { bowl });
      player.updateInventory();
    }
    if ((item.equals(Material.PORK)) 
    		|| (item.equals(Material.RAW_FISH)) 
    		|| (item.equals(Material.RAW_BEEF)) 
    		|| (item.equals(Material.RAW_CHICKEN)) 
    		|| (item.equals(Material.ROTTEN_FLESH))) 
    {
      Bukkit.broadcastMessage("Nauseate Player");
      nauseatePlayer(player);
    }
	  event.setUseItemInHand(Event.Result.DENY);
	  player.getInventory().remove(Material.getMaterial(itemName));
  }
  private void setHealth(Player player, double health) 
  {
    double newHp = Math.min(20.0, player.getHealth() + health);
    player.setHealth(newHp);
    Bukkit.broadcastMessage("setHealth");
  }
  private boolean eatingAllowed(Player player, double health) 
  {
    if ((health > 0.0) && (player.getHealth() >= 20.0)) 
    {
      return false;
    }
    else if ((health < 0.0) && (player.getHealth() <= 0.0))
    {
      return false;
    }
    else if ((health > 0.0) && (player.getHealth() < 20.0)){
      return true;
    }
    return true;
  }
  
  @EventHandler
  private void onPlayerRightClickCake(PlayerInteractEvent event) 
  {
	Player player = event.getPlayer();
    double health = Main.getCakeHealth();
    if ((health == 0.0) || (!eatingAllowed(player, health)))
    {
      return;
    }
    eatCake(event.getClickedBlock());
    setHealth(player, health);
  }

  @SuppressWarnings("deprecation")
private void eatCake(Block block) 
  {
    byte eaten = block.getData();
    if (eaten == 5)
      block.setType(Material.AIR);
    else
      block.setData((byte)(eaten + 1)); 
  }

  private void nauseatePlayer(Player player) 
  {
      CraftPlayer cp = (CraftPlayer)player;
      cp.getHandle().addEffect(new MobEffect(MobEffectList.CONFUSION.id, 20 * 20, 3));
      cp.getHandle().addEffect(new MobEffect(MobEffectList.HUNGER.id, 20 * 20, 1));
    } 
  
  @EventHandler(priority=EventPriority.NORMAL)
  private void onPlayerJoin(PlayerJoinEvent event) 
  {
	  Player player = event.getPlayer();
	  if (player.getFoodLevel() < 20) 
	  {
		  player.setFoodLevel(20);
	  }
  }
  
}